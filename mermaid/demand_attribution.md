```mermaid
graph TD

B1[Product View Only]
B1 -->|viewed| C1[fa:fa-eye Product Page]
C1 .->D1((Search))

B[Product View and Add to Bag]
B -->|viewed| C[fa:fa-eye Product Page]
C -->|added| D[fa:fa-shopping-bag Add to bag]
D -->|ordered| E[fa:fa-check-circle Order]
E .-> |Same Day Orders| F((Search))
E .-> |Non-Same Day Orders| G((Unknown))


B2[No Product View and Add to Bag]
B2 .->|e.g. viewed from Looks in PDP| C2>fa:fa-ban No Product View]
C2 .->|added| D2[fa:fa-shopping-bag Add to bag]
click C2 """This is a tooltip for a link"
D2 -->|ordered| E2[fa:fa-check-circle Order]

E2 --> F2((Looks))
```