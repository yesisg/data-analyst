```mermaid
graph LR
t1[Data Source] --- t2[Data Source Filters]
t2 --- t3[Context Filters ]
t3 ---t4
t4{Fixed}
t4 --- t6[Dimensions Filters]
t8{Include}
t9{Exclude}
t6 ---t8
t6 --- t9
t8 --- t10[Measure Filters]
t9 --- t10[Measure Filters]

style t1 fill:#c2d6d6, stroke:#669999, stroke-width:3;
style t2 fill:#c2d6d6, stroke:#FFF;
style t3 fill:#c2d6d6, stroke:#FFF;
style t6 fill:#c2d6d6, stroke:#FFF;
style t10 fill:#c2d6d6, stroke:#FFF;
```
