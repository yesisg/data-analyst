```mermaid

graph LR
t1(Digital Workflow & Process)
t0[Swimlane<br> Partners] ---|requests<br>prioritized| t3{Sprint<br> Planning}
n1[Work is estimated at a sprint level.<br>Each sprint is 2-weeks or 8 story pts. <br> Work makes up about 75% of our sprints. <br> Other 25% is allocated to Operational Sustainment and Growth]

%% Work estimation
eta1[fa:fa-battery-empty Explore - 10%] .-> |Additional time may <br> be needed for bugs fixes|eta2[fa:fa-battery-quarter Code - 30%] 
eta2 .-> eta3[fa:fa-battery-half Analyze - 20%<br>fa:fa-battery-half Production - 10%]  
eta3 .-> eta4[fa:fa-battery-three-quarters DA Output - 10% <br> fa:fa-battery-three-quarters BI Output - 20%] 
eta4 .-> eta5[fa:fa-battery-full Deliverable - 5%] 

%% Digital Teams
subgraph "Digital Teams"
t3 --- bi8
t3 --- da8
bi8[Business<br>Intelligence]
da8[Digital<br>Analytics]
end

%% BI
subgraph "BI Process" 
%% Process
bi8 ---bi11[fa:fa-battery-empty Explore]
bi11---bi12[fa:fa-battery-quarter Code]
bi12 --- bi13[fa:fa-battery-half Production]
bi13 --- bi14[fa:fa-battery-three-quarters Output]
bi14 --- bi10[fa:fa-battery-full Deliverable]
%% Details
bi11 .- bi15[Understand the business ask, <br> data availability and quality.]
bi12 .- bi16[Extract, Transform, Load. <br> Code testing and QA <br> is done in this step. <br><br> Tools: SQL, Python]
bi13 .- bi17[ETL goes through code review <br> and prepared for automation. <br><br> Tools: SQL, Luigi, Tableau]
bi14 .- bi18[Design, build and publish dashboard. <br> Teams review in BETA and provide<br>feedback before finalizing deliverable. <br><br> Tools: Tableau, SQL]
%% Note
bin1[Automated Reporting & <br>Enhancements]
end

%% DA
subgraph "DA Process"
%% Process
da8---da11[fa:fa-battery-empty Explore] 
da11 ---da12[fa:fa-battery-quarter Code]
da12 --- da13[fa:fa-battery-half  Analyze]
da13 --- da14[fa:fa-battery-three-quarters  Output]
da14 --- da10[fa:fa-battery-full Deliverable]
%% Details
da11 .- da15[Understand the business ask, <br> data availability and quality.]
da12 .- da16[Extract, Transform, Load. <br> Code testing and QA <br> is done in this step. <br><br>Tools: SQL]
da13 .- da17[Pipe data to perform <br>analyses, including modeling and<br> summary statistics.<br><br> Tools: SQL, R, Python, Excel]
da14 .- da18[Generate interactive or <br>printable brief explicating <br>and summarizing findings. <br><br> Tools: R, Python, HTML]
%% Note
dan1[Adhoc &<br> Analysis briefings]
end

%% Styles: Digital Teams 
style bi8 fill:#c2d6d6, stroke:#FFF;
style da8 fill:#c2d6d6, stroke:#FFF;

%% Styles: DA subgraph workflow
style da10 fill:#c2d6d6, stroke:#FFF;
style da11 fill:#c2d6d6, stroke:#FFF;
style da12 fill:#c2d6d6, stroke:#FFF;
style da13 fill:#c2d6d6, stroke:#FFF;
style da14 fill:#c2d6d6, stroke:#FFF;

%% Styles: BI subgraph workflow
style bi10 fill:#c2d6d6, stroke:#FFF;
style bi11 fill:#c2d6d6, stroke:#FFF;
style bi12 fill:#c2d6d6, stroke:#FFF;
style bi13 fill:#c2d6d6, stroke:#FFF;
style bi14 fill:#c2d6d6, stroke:#FFF;

%% Styles: Work Estimation
style eta1 fill:#c2d6d6, stroke:#FFF;
style eta2 fill:#c2d6d6, stroke:#FFF;
style eta3 fill:#c2d6d6, stroke:#FFF;
style eta4 fill:#c2d6d6, stroke:#FFF;
style eta5 fill:#c2d6d6, stroke:#FFF;

%% Styles: Other
style t0 fill:#c2d6d6, stroke:#FFF;
style t1 fill:#c2d6d6, stroke:#669999, stroke-width:3;
style t3 fill:#c2d6d6, stroke:#FFF;
style n1 fill:#f2f6f2, stroke:#FFF;
style bin1 fill:#f2f6f2, stroke:#FFF;
style dan1 fill:#f2f6f2, stroke:#FFF;
```