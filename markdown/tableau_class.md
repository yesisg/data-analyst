---
layout: wiki
title: TUC@N
toc: true
category: tableau data-visual best-practices visuals graphs
---

## Introduction
Tableau User Community @ Nordstrom (TUC@N) is an internal class available to Nordstrom employees and is led by Yesi Garcia and Sarah Dershowitz. The purpose of this class is to give you an introduction to Tableau and help you get started with the product. 

{% asset tableau-tucan-logo.png %}

## About the Class
The chances are, you are coming to this class because you see that Tableau could provide you and your team with value. Whether your needs stem from reporting, tracking, analytics, or dashboarding you want to see if Tableau will suffice. 

This class will be set up and instruction will be twice a week for 1 hour and 15 minutes. The first hour is purely instruction and the remaining 15 minutes will be set up as a Q&A.

```mermaid
gantt
title Course Breakout
dateformat YYYY-MM-DD
axisFormat %M
Instruction :  active, 2018-11-01, 60m 
Q&A :  15m  
```


> Need a bit more help? Here are additional Tableau resources:
- Slack: #Tableau
- Tableau User Group in 865 every Tuesday at 11a

## Course Segments
The first class will be an introduction on the product. The first segment of this class will be connecting to your data, the second segment will be best practices in data visualization, and the third segment will be workbook and publishing best practices.

In all of the following segments you'll learn and apply best practices using Nordstrom data and data sources.

### Connecting to Data at Nordstrom
- Data Sources
	- Connecting to Servers
	- Live vs Extracts
	- Popular Databases, Files etc at Nordstrom
	- Tableau Data Sources
		- Cummulative
		- Non-Cummulative
		- Best Practices
- Blends vs Joins
- Features
	- Filtering from Data Source
	- Naming Data Source


### Calculations (Sorcery!)
- Creating Fields
	- Calculated Fields
	- Parameters
	- Combined Fields
	- Table Calculations
- Relationships between fields
	- Hieararchies
	- Groups
	- Sets
	- Parameters + Calculated Fields
- LODs
- Statistical Capabilities

### Data Visualization

- Charts
	- Area Charts
	- Tree maps
	- Spread and Box Plot Alternatives 
	- Proportions and Pie Chart Alternatives
	- Line Charts and Time Series
	- Heat Maps
	- Highlight Tables
	- Gant Charts
	- Pareto Charts
	- ‘Crosstabs’
	- Maps

- Style
	- Colors
	- Text
	- Titles and Aliases
	- Axes
	- Labels
	- Formatting Measures
	- Context - % totals, totals
	- Custom shapes and colors

- Features
	- Utilizing Shapes and Color – creating indicators
	- Dual Axes
	- Hierarchies
	- Tooltips
	- Sorting
	- Viz in Tooltips
	- Filtering across Sheets
	- Groups
	- Pages
	- Actions

### Workbook and Publishing

- Workbook Features
	- TWB or TWBX
	- Raw Data Download
	- Downloading VS Open from Server
	- Actions 
	- Filtering in Action
	- Section 
- Dashboard Style Guides
	- Fonts
	- Text Boxes
	- Logos
	- Tiled vs Floating

- Publishing Best Practices
	- Comments
	- Naming 
	- Keeping Tidy and Organized
		- Color Coding
		- Hiding Unused Fields 
		- Titling Sheets and Dashboards
		- FAQ Page


## Meet the Instructors
This class is led by Yesi Garcia and Sarah Dershowitz. Yesi has been with Nordstrom for over two years on Digital BI, she has helped pioneer Tableau at Nordstrom as a power user and administrator and leads a Tableau User Group (TUG) weekly in 865. Sarah worked at tableau for 3 years as a power user on the Sales Analytics team before coming to Nordstrom. Together they have created TUC@N - Tableau User Community @ Nordstrom.
