import json
import requests
# import nordypy
import pandas as pd
from pandas.io.json import json_normalize
from pandas import DataFrame
import boto3
import csv

## nordypy connection details

## zips bucket endpoint
zips = 'https://s3-us-west-2.amazonaws.com/eta-local-zips-and-stores/localZipsAndStores.json'

# raw
r = requests.get(url=f"{zips}")
raw = r.json()

## return list of zips in dataframe
df = pd.DataFrame(raw['zips'])

## export to csv
df.to_csv('/Users/awya/queries/lme-zips/la_lme_zips.csv',index=False)
with open('/Users/awya/queries/lme-zips/la_lme_zips.csv','rb') as csv_file:
    csvwriter = csv.writer(csv_file,delimiter=',')
