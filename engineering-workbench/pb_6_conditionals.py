
# if - runs if conditional is true
if language == 'Python':
  print('Conditional was True')

# if, elif, else
if language == 'Python':
  print('Language is Python.')
elif language == 'Java':
  print('Language is Java')
else:
  print('No match')

# comparitive operators
user = 'Admin'
logged_in = True

if user == 'Admin' and logged_in:
  print('Admin Page')
else:
  print('Bad Creds')

# object identity
a = [1, 2, 3]
b = [1, 2, 3]

print(id(a))
print(id(b))
print(a is b)

# condition
"""
False
None
Zero of any numeric type
Any empty sequence. For example, '', (), []
Any empty mapping. For example, {}
"""
condition = 0

if condition:
  print('Evaluated to True')
else:
  print('Evaluated to False')