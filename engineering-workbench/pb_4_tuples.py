# TUPLES
courses = ['History', 'Math', 'Physics', 'CompSci']

# MUTABLE List []
list_1 = ['History', 'Math', 'Physics', 'CompSci']
list_2 = list_1

print(list_1)
print(list_2)

# changes value at index 0
# if you want the list of values to change, this is the way
list_1[0] = 'Art'

print(list_1)
print(list_2)



""" TUPLES ()
If you want a list of values that know are not going to change
use TUPLES.
"""

tuple_1 = ('History', 'Math', 'Physics', 'CompSci')
tuple_2 = tuple_1

print(tuple_1)
print(tuple_2)

# Error: because Tuples are immutable
tuple_1[0] = 'Art'

print(tuple_1)
print(tuple_2)
