
message = 'hello world'
greeting = 'hello'

# pre 3.6 format
message = '{}, {}. Welcome!'.format(greeting, name)

# f strings
message = f'{greeting}, {name}. Welcome!'


# options available
print(dir(name))

# details on available
print(help(str))