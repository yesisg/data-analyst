--Temp tables declaring DATE variables throughout stack: Redshift & Teradata

 /*---------------------------------------------------------------
   REDSHIFT - Temp table declaring DATE variables throughout stack
 ---------------------------------------------------------------*/
	DROP TABLE IF EXISTS date_range;
	CREATE TEMPORARY TABLE date_range AS
	 (
		SELECT
		  dateadd(DAY, -8, date_trunc('week', convert_timezone('US/Pacific', getdate())))AS startdate --WeekStart
		, dateadd(DAY, -2, date_trunc('week', convert_timezone('US/Pacific', getdate()))) AS enddate --WeekEnd
		-- '2017-07-15' AS startdate --StartDate
 		--, '2017-07-25' AS enddate --EndDate
	 );

	--calling date range in SELECT
	SELECT * FROM [table]
	WHERE [dates] BETWEEN (SELECT date_range.startdate FROM date_range) AND (SELECT date_range.enddate FROM date_range);


	DROP TABLE date_range;


 /*---------------------------------------------------------------
   TERADATA - Temp table declaring DATE variables throughout stack
 ---------------------------------------------------------------*/
	CREATE SET VOLATILE TABLE date_range AS
	(
	SELECT
	 dateadd(DAY, -8, date_trunc('week', CURRENT_DATE)) AS startdate --WeekStart
	, dateadd(DAY, -2, date_trunc('week', CURRENT_DATE)) AS enddate --WeekEnd
	--'2017-03-09' AS startdate
	--, '2017-03-22' AS enddate
	--, '2016-10-11 00:00:00.000001' as StartTime
	--, '2016-04-30 16:40:00.000000' as EndTime
	)
	WITH DATA
	PRIMARY INDEX (startdate, enddate)
	ON COMMIT PRESERVE ROWS
	;

	--calling date range in SELECT
	SELECT * FROM [table]
	WHERE [dates] BETWEEN (sel date_range.startdate FROM date_range) AND (sel date_range.enddate FROM date_range);

	DROP TABLE date_range;


