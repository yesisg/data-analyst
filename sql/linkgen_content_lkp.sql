/*
------------------------------------------------
 LINKGEN CONTENT LOOKUP
 Linkgen tables joined to serve as master lookup, primarily, for Site Promo

    About Linkgen
    Linkgen serves as a tool for content team to manage campaign/promo content; most commonly used for site and email content.
    i. UI - http://linkgen/MyWork?type=jwn

    About Tags
    Sample query: campaign=-_-&jid=J009126&cid=-_-&cm_sp=merch-_-corp_1952_J009126-_-hp_corp_P04_info
    All after 'cm_sp' flows into sp_site_promos: cm_sp=merch-_-corp_1952_J009126-_-hp_corp_P04_info
    Linkgen ID placed between '_': corp_1952_J009126 >> 1952
    JWNID (ID to track content groups): jid=J009126
        i. Linkgen content tagging began ~June 26, 2017;
        i. Tag issues found in July around Anniversary;
        i. Not all clk_strm_sp.sp_site_promos records have Linkgen applicable ID's to parse, parse anyway

    How to derive ID from clk_strm_sp.sp_site_promos:
        1. Parse id from cm_sp promotion tags >> split_part(promotion, '_', 2)
        2. Join on project_id

 Tables used:
    - [a] cust_vws.projects
    - [b] cust_vws.jwnid
    - [c] cust_vws.deliverable
    - [d] cust_vws.segments

 Definitions
    - [b] program_nm -- promotion/campaign program set by linkgen; can be null
    - [a] project_nm -- name of content
    - [c] deliverable_nm -- placement detail on page
    - [a] projectstart -- start date of content on page
    - [a] projectend -- end date of content on page
    - [a] project_id -- content identifier to be tied back to clk_strm_sp.sp_site_promos
    - [a] jwnid -- content identifier to be tied to program information
    - [b] programstart -- start date of program
    - [b] programend -- end date of program
    - [d] segment -- targeted division: brand, corp, multi, kids, beauty, etc
------------------------------------------------
*/
DROP TABLE IF EXISTS bi_prd.linkgen_content_lkp ;
--[[bi_prd.linkgen_content_lkp]]
CREATE TABLE bi_prd.linkgen_content_lkp
diststyle KEY
distkey(project_id)
sortkey(project_id)
AS
(
    SELECT
      b.name program_nm
    , a.projectname project_nm
    , c.name deliverable_nm
    , a.projectstart
    , a.projectend
    , a.id project_id
    , b.id jwnid
    , b.startdate programstart
    , b.enddate programend
    , d.name segment

    FROM cust_vws.projects a

    LEFT JOIN cust_vws.jwnid b ON a.jwnid = b.id    
    LEFT JOIN cust_vws.deliverable c ON a.id = c.projectid
    LEFT JOIN cust_vws.segments d ON b.segment = d.id
)
;

GRANT SELECT ON bi_prd.linkgen_content_lkp TO PUBLIC;


