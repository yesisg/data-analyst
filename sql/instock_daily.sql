--INSTOCK DAILY
/*
    How to use this table:
    JOIN ON activity_date and style_group_idnt

    Examples can be found in Category/Breadcrumb/Style Group Funnel Daily tables
*/

/*
---------------------------------------------------------------
   Temp table declaring DATE variables throughout stack
---------------------------------------------------------------
*/
DROP TABLE IF EXISTS instock_date_range;
--[[bi_prd.instock_daily]]
CREATE TEMPORARY TABLE instock_date_range AS (
    SELECT
     trunc(convert_timezone('PST8PDT', GETDATE())) - 15 AS derivedstart --derived_date start
   , trunc(convert_timezone('PST8PDT', GETDATE())) - 1 AS derivedend --derived_date end
);

/*
------------------------------
This gets the style-group-level in-stock information. We combine
this with SKU-level information later on.
------------------------------
*/
DROP TABLE IF EXISTS instock_pt_1;

CREATE TEMPORARY TABLE instock_pt_1
diststyle KEY
distkey(style_group_idnt)
sortkey(business_date)
AS
(
    SELECT
        a.style_group_idnt
        , a.business_date
        , a.total_prod_vws
        , a.sty_grp_ord_qty_cnt
        , CASE WHEN (coalesce(sum(sku_ord_qty_cnt), 0) = 0) THEN 0 ELSE 1 END AS sku_flag
        , count(coalesce(a.sku_idnt, 'wubbalubadubdub')) AS sku_count

    FROM cust_vws.instk_sku_sg_fct a

    WHERE
        a.business_date BETWEEN (SELECT derivedstart FROM instock_date_range) AND (SELECT derivedend FROM instock_date_range)
        AND style_group_idnt IS NOT NULL
        AND total_prod_vws IS NOT NULL
        AND sty_grp_ord_qty_cnt IS NOT NULL

    GROUP BY 1,2,3,4
);

/*
------------------------------
And this is the SKU-level info I mentioned.

NOTE: At this time, the RP lookup table only contains
current RP mapping. We can't see a point in time of our
RP position until D&S decides it matters.
------------------------------
*/
DROP TABLE IF EXISTS instock_pt_2;

CREATE TEMPORARY TABLE instock_pt_2
diststyle KEY
distkey(style_group_idnt)
sortkey(business_date)
AS
(
    SELECT
        i.style_group_idnt
        , i.sku_idnt
        , i.business_date
        , coalesce(sku_ord_qty_cnt, 0) AS sku_ord_qty_cnt
        , CASE WHEN f_i_repl_actv_flag = 'Y' THEN 'RP' ELSE 'NR' END AS rp_idnt
        , sum(in_stock_vws) AS in_stock_vws

    FROM
        cust_vws.instk_sku_sg_fct i
        LEFT JOIN cust_vws.rp_sku_loc_cur_lkp skulkps
            ON i.sku_idnt = skulkps.sku_idnt
            AND skulkps.loc_idnt = '808'

    WHERE
        i.business_date BETWEEN (SELECT derivedstart FROM instock_date_range) AND (SELECT derivedend FROM instock_date_range)
        AND i.style_group_idnt IS NOT NULL

    GROUP BY 1,2,3,4,5
);

/*
------------------------------
This rolls the SKU- and SG-level info together.
If we have SKU-level views, we use them, otherwise
we split and weight the SG-level views across the
size/color run
------------------------------
*/
DROP TABLE IF EXISTS instock_pt_3;

CREATE TEMPORARY TABLE instock_pt_3
diststyle KEY
distkey(style_group_idnt)
sortkey(business_date)
AS
(
    SELECT
        b.style_group_idnt
        , i.sku_idnt
        , i.business_date
        , i.rp_idnt
        , i.in_stock_vws
        , sum(CASE WHEN b.sku_flag = 0 THEN b.total_prod_vws/b.sku_count
                    WHEN sty_grp_ord_qty_cnt > 0 THEN sku_ord_qty_cnt/sty_grp_ord_qty_cnt*total_prod_vws
                    ELSE 0 END) AS product_views

    FROM
        instock_pt_2 i
        JOIN instock_pt_1 b
            ON i.style_group_idnt = b.style_group_idnt
            AND i.business_date = b.business_date

    GROUP BY 1,2,3,4,5
);

/*
------------------------------
This just pivots the data from instock_3 into RP/Non-RP columns.
------------------------------
*/
DROP TABLE IF EXISTS instock_temp_staging;

CREATE TEMPORARY TABLE instock_temp_staging
diststyle KEY
distkey(style_group_idnt)
sortkey(business_date)
AS
(
    SELECT
        business_date
        , style_group_idnt
        , sum(CASE WHEN rp_idnt = 'RP' THEN product_views ELSE 0 END)/sum(product_views) AS rp_prod_share
        , sum(CASE WHEN rp_idnt = 'NR' THEN product_views ELSE 0 END)/sum(product_views) AS nrp_prod_share
        , CASE WHEN sum(in_stock_vws) = 0 THEN 0 ELSE sum(CASE WHEN rp_idnt = 'RP' THEN in_stock_vws ELSE 0 END)/sum(product_views) END AS rp_stock_share
        , CASE WHEN sum(in_stock_vws) = 0 THEN 0 ELSE sum(CASE WHEN rp_idnt = 'NR' THEN in_stock_vws ELSE 0 END)/sum(product_views) END AS nrp_stock_share
        , sum(CASE WHEN rp_idnt = 'RP' THEN product_views ELSE 0 END) AS rp_views
        , sum(CASE WHEN rp_idnt = 'NR' THEN product_views ELSE 0 END) AS nrp_views
        , sum(CASE WHEN rp_idnt = 'RP' THEN in_stock_vws ELSE 0 END) AS rp_instock_views
        , sum(CASE WHEN rp_idnt = 'NR' THEN in_stock_vws ELSE 0 END) AS nrp_instock_views
        , sum(in_stock_vws) AS instock

    FROM instock_pt_3

    GROUP BY 1,2
);

/*
------------------------------
Now we just need to clear out these dates from the final table
------------------------------
*/
DELETE FROM bi_prd.instock_daily
WHERE bi_prd.instock_daily.activity_date >= (SELECT derivedstart FROM instock_date_range)
AND bi_prd.instock_daily.activity_date <= (SELECT derivedend FROM instock_date_range)
;


/*
------------------------------
And finally, we insert the data into the final table.
------------------------------
*/
INSERT INTO bi_prd.instock_daily
(
    activity_date
    , style_group_idnt
    , rp_prod_share
    , nrp_prod_share
    , rp_stock_share
    , nrp_stock_share
    , rp_views
    , nrp_views
    , rp_instock_views
    , nrp_instock_views
    , instock_views
)
SELECT
        business_date AS activity_date
        , style_group_idnt
        , sum(rp_prod_share) AS rp_prod_share
        , sum(nrp_prod_share) AS nrp_prod_share
        , sum(rp_stock_share) AS rp_stock_share
        , sum(nrp_stock_share) AS nrp_stock_share
        , sum(rp_views) AS rp_views
        , sum(nrp_views) AS nrp_views
        , sum(rp_instock_views) AS rp_instock_views
        , sum(nrp_instock_views) AS nrp_instock_views
        , sum(rp_instock_views) + sum(nrp_instock_views) AS instock_views

    FROM instock_temp_staging

    GROUP BY 1, 2
;
